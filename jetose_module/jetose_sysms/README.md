# jetose_sysms
jetose_功能模块_系统管理

## 使用指南
### 作为依赖包使用
- 生成jar包
```
mvn clean package
```
- 上传到本地maven仓库
```
mvn install:install-file -Dfile=jetose_sysms-2017.0.0.1.jar -DgroupId=com.jetose -DartifactId=jetose_sysms -Dversion=2017.0.0.1 -Dpackaging=jar
```
### 作为运行包使用
- 拷贝所需模块的sql文件到resources/db/migration
- 调整文件版本号
- 打开pom文件注释部分
- 生成jar包
```
mvn clean package
```
- 运行jar包
```
java -jar jetose_sysms.jar
```

## 模块依赖
- jetose_springboot -- jetose_核心模块_以spring-boot为核心搭建

## 开发进度
- v2017.0.0.1 -- 完成用户信息相关api开发