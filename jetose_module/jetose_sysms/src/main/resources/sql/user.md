sample
===
* 注释

	select #use("cols")# from SYS_USER where #use("condition")#

cols
===

	user_id,account,password,name,sex,remark,create_id,create_time

updateSample
===

	`user_id`=#userId#,`account`=#account#,`password`=#password#,`name`=#name#,`sex`=#sex#,`remark`=#remark#,`create_id`=#createId#,`create_time`=#createTime#

condition
===

	1 = 1  
	@if(!isEmpty(account)){
	 and `account`=#account#
	@}
	@if(!isEmpty(password)){
	 and `password`=#password#
	@}
	@if(!isEmpty(name)){
	 and `name`=#name#
	@}
	@if(!isEmpty(sex)){
	 and `sex`=#sex#
	@}
	@if(!isEmpty(remark)){
	 and `remark`=#remark#
	@}
	@if(!isEmpty(createId)){
	 and `create_id`=#createId#
	@}
	@if(!isEmpty(createTime)){
	 and `create_time`=#createTime#
	@}
	
queryByPage
===

    select 
    @pageTag() {
        #use("cols")#
    @}
    from SYS_USER 
    where #use("condition")# 
	
