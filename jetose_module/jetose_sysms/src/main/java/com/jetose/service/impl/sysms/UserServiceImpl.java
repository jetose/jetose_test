package com.jetose.service.impl.sysms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.sysms.User;
import com.jetose.mapper.sysms.UserMapper;
import com.jetose.service.sysms.UserService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    public PageQueryDto queryByPage(PageQuery pageQuery) {
        PageQueryDto pageQueryDto = new PageQueryDto();
        userMapper.queryByPage(pageQuery);
        pageQueryDto.setTotalRow(pageQuery.getTotalRow());
        pageQueryDto.setData(pageQuery.getList());
        return pageQueryDto;
    }

    public void save(User user) {
        userMapper.insert(user);
    }

    public int modify(User user) {
        return userMapper.updateTemplateById(user);
    }

    public int remove(String id) {
        return userMapper.deleteById(id);
    }

    public User detail(String id) {
        return userMapper.single(id);
    }
}
