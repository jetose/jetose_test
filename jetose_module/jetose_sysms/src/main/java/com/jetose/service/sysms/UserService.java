package com.jetose.service.sysms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.sysms.User;
import org.beetl.sql.core.engine.PageQuery;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface UserService {

    PageQueryDto queryByPage(PageQuery pageQuery);

    void save(User user);

    int modify(User user);

    int remove(String id);

    User detail(String id);

}
