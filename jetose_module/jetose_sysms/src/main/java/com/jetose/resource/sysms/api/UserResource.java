package com.jetose.resource.sysms.api;

import com.jetose.annotation.PageQueryAnnotation;
import com.jetose.dto.PageQueryDto;
import com.jetose.entity.sysms.User;
import com.jetose.service.sysms.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/apis/v1/users")
@Api(value = "incomes", description = "用户信息apis")
public class UserResource {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "用户信息查询", notes = "查询用户信息相关信息，模糊匹配，分页显示")
    @ApiImplicitParams({            @ApiImplicitParam(paramType = "query", name = "pageNumber", value = "当前页数", required = true, defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页记录", required = true, defaultValue = "20", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "paras", value = "查询参数", required = false, dataType = "string")
    })
    public PageQueryDto queryByPage(@PageQueryAnnotation @ApiIgnore PageQuery pageQuery) {
        return userService.queryByPage(pageQuery);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "用户信息新增", notes = "新增用户信息相关信息")
    public User save(@RequestBody User user) {
        userService.save(user);
        return user;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "用户信息编辑", notes = "编辑用户信息相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int modify(@PathVariable("id") String id, @RequestBody User user) {
        user.setUserId(id);
        return userService.modify(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "用户信息删除", notes = "删除用户信息相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int remove(@PathVariable("id") String id) {
        return userService.remove(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "用户信息详情", notes = "查询用户信息详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public User detail(@PathVariable("id") String id) {
        return userService.detail(id);
    }

}
