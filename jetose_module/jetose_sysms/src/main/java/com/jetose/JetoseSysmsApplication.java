package com.jetose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017/9/18.
 */
@SpringBootApplication
public class JetoseSysmsApplication {

    public static void main(String[] args){
        SpringApplication.run(JetoseSysmsApplication.class, args);
    }

}
