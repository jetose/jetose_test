package com.jetose.entity.sysms;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;

import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "SYS_USER")
@ApiModel(value = "User", description = "用户信息表对应实体类对象")
public class User implements Serializable{

	@AssignID("uuid")
	@ApiModelProperty(value = "主键ID", dataType = "string", required = true, position = 1)
	private String userId ;

	@ApiModelProperty(value = "用户名称", dataType = "string", required = true, position = 2)
	private String name ;

	@ApiModelProperty(value = "用户性别", dataType = "string", required = true, position = 3)
	private String sex ;

	@ApiModelProperty(value = "登录账号", dataType = "string", required = true, position = 4)
	private String account ;

	@ApiModelProperty(value = "登录密码", dataType = "string", required = true, position = 5)
	private String password ;

	@ApiModelProperty(value = "用户备注", dataType = "string", required = true, position = 6)
	private String remark ;

	@ApiModelProperty(value = "操作人", dataType = "string", required = true, position = 7)
	private String createId ;

	@ApiModelProperty(value = "操作时间", dataType = "date", required = true, position = 8)
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date createTime ;

}
