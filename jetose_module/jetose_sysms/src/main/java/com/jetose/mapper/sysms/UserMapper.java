package com.jetose.mapper.sysms;

import com.jetose.entity.sysms.User;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface UserMapper extends BaseMapper<User> {

    void queryByPage(PageQuery pageQuery);

}
