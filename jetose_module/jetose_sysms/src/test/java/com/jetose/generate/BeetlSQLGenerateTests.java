package com.jetose.generate;

import com.jetose.JetoseSysmsApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetoseSysmsApplication.class)
@WebAppConfiguration
@Slf4j
public class BeetlSQLGenerateTests {

    @Autowired
    private BeetlSQLGenerate beetlSQLGenerate;

    @Test
    public void getBeetlSQLGenerateTest() throws Exception {
        beetlSQLGenerate.getBeetlSQLGenerate();
    }
}
