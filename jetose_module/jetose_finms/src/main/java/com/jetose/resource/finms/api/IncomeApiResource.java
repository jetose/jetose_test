package com.jetose.resource.finms.api;

import com.jetose.annotation.PageQueryAnnotation;
import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Income;
import com.jetose.service.finms.IncomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/apis/v1/incomes")
@Api(value = "incomes", description = "收入明细apis")
public class IncomeApiResource {

    @Autowired
    private IncomeService incomeService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "收入明细查询", notes = "查询收入明细相关信息，模糊匹配，分页显示")
    @ApiImplicitParams({            @ApiImplicitParam(paramType = "query", name = "pageNumber", value = "当前页数", required = true, defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页记录", required = true, defaultValue = "20", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "paras", value = "查询参数", required = false, dataType = "string")
    })
    public PageQueryDto queryByPage(@PageQueryAnnotation @ApiIgnore PageQuery pageQuery) {
        return incomeService.queryByPage(pageQuery);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "收入明细新增", notes = "新增收入明细相关信息")
    public Income save(@RequestBody Income income) {
        incomeService.save(income);
        return income;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "收入明细编辑", notes = "编辑收入明细相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int modify(@PathVariable("id") String id, @RequestBody Income income) {
        income.setIncomeId(id);
        return incomeService.modify(income);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "收入明细删除", notes = "删除收入明细相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int remove(@PathVariable("id") String id) {
        return incomeService.remove(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "收入明细详情", notes = "查询收入明细详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public Income detail(@PathVariable("id") String id) {
        return incomeService.detail(id);
    }

}
