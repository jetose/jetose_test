package com.jetose.resource.finms.api;

import com.jetose.annotation.PageQueryAnnotation;
import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Outgo;
import com.jetose.service.finms.OutgoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/apis/v1/outgos")
@Api(value = "incomes", description = "支出明细apis")
public class OutgoApiResource {

    @Autowired
    private OutgoService outgoService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "支出明细查询", notes = "查询支出明细相关信息，模糊匹配，分页显示")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "pageNumber", value = "当前页数", required = true, defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页记录", required = true, defaultValue = "20", dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "paras", value = "查询参数", required = false, dataType = "string")
    })
    public PageQueryDto queryByPage(@PageQueryAnnotation @ApiIgnore PageQuery pageQuery) {
        return outgoService.queryByPage(pageQuery);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "支出明细新增", notes = "新增支出明细相关信息")
    public Outgo save(@RequestBody Outgo outgo) {
        outgoService.save(outgo);
        return outgo;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "支出明细编辑", notes = "编辑支出明细相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int modify(@PathVariable("id") String id, @RequestBody Outgo outgo) {
        outgo.setOutgoId(id);
        return outgoService.modify(outgo);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "支出明细删除", notes = "删除支出明细相关信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public int remove(@PathVariable("id") String id) {
        return outgoService.remove(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "支出明细详情", notes = "查询支出明细详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "id", value = "主键ID", required = true, dataType = "string")
    })
    public Outgo detail(@PathVariable("id") String id) {
        return outgoService.detail(id);
    }

}
