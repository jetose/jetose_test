package com.jetose.mapper.finms;

import com.jetose.entity.finms.Income;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface IncomeMapper extends BaseMapper<Income> {

    void queryByPage(PageQuery pageQuery);

}
