package com.jetose.mapper.finms;

import com.jetose.entity.finms.Outgo;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface OutgoMapper extends BaseMapper<Outgo> {

    void queryByPage(PageQuery pageQuery);

}
