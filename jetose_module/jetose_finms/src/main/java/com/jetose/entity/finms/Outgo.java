package com.jetose.entity.finms;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "FIN_OUTGO")
@ApiModel(value = "Outgo", description = "支出明细表对应实体类对象")
public class Outgo {

	@AssignID("uuid")
	@ApiModelProperty(value = "主键ID", dataType = "string", required = true, position = 1)
	private String outgoId ;

	@ApiModelProperty(value = "支出日期", dataType = "date", required = true, position = 2)
	@JSONField(format="yyyy-MM-dd")
	private Date outgoDate ;

	@ApiModelProperty(value = "支出金额", dataType = "long", required = true, position = 3)
	private BigDecimal amount ;

	@ApiModelProperty(value = "支出备注", dataType = "string", position = 4)
	private String remark ;

	@ApiModelProperty(value = "操作人", dataType = "string", required = true, position = 5)
	private String createId ;

	@ApiModelProperty(value = "操作时间", dataType = "date", required = true, position = 6)
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date createTime ;

}
