package com.jetose.entity.finms;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "FIN_INCOME")
@ApiModel(value = "Income", description = "收入明细表对应实体类对象")
public class Income {

	@AssignID("uuid")
	@ApiModelProperty(value = "主键ID", dataType = "string", required = true, position = 1)
	private String incomeId ;

	@ApiModelProperty(value = "收入日期", dataType = "date", required = true, position = 2)
	@JSONField(format="yyyy-MM-dd")
	private Date incomeDate ;

	@ApiModelProperty(value = "收入金额", dataType = "long", required = true, position = 3)
	private BigDecimal amount ;

	@ApiModelProperty(value = "收入备注", dataType = "string", position = 4)
	private String remark ;

	@ApiModelProperty(value = "操作人", dataType = "string", required = true, position = 5)
	private String createId ;

	@ApiModelProperty(value = "操作时间", dataType = "date", required = true, position = 6)
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date createTime ;

}
