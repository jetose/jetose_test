package com.jetose.service.finms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Outgo;
import org.beetl.sql.core.engine.PageQuery;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface OutgoService {

    PageQueryDto queryByPage(PageQuery pageQuery);

    void save(Outgo outgo);

    int modify(Outgo outgo);

    int remove(String id);

    Outgo detail(String id);

}
