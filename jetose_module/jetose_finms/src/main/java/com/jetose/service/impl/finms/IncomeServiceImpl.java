package com.jetose.service.impl.finms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Income;
import com.jetose.mapper.finms.IncomeMapper;
import com.jetose.service.finms.IncomeService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IncomeServiceImpl implements IncomeService {

    @Autowired
    private IncomeMapper incomeMapper;

    public PageQueryDto queryByPage(PageQuery pageQuery) {
        PageQueryDto pageQueryDto = new PageQueryDto();
        incomeMapper.queryByPage(pageQuery);
        pageQueryDto.setTotalRow(pageQuery.getTotalRow());
        pageQueryDto.setData(pageQuery.getList());
        return pageQueryDto;
    }

    public void save(Income income) {
        incomeMapper.insert(income);
    }

    public int modify(Income income) {
        return incomeMapper.updateTemplateById(income);
    }

    public int remove(String id) {
        return incomeMapper.deleteById(id);
    }

    public Income detail(String id) {
        return incomeMapper.single(id);
    }
}
