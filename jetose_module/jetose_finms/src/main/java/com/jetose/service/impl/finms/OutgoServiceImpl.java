package com.jetose.service.impl.finms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Outgo;
import com.jetose.mapper.finms.OutgoMapper;
import com.jetose.service.finms.OutgoService;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OutgoServiceImpl implements OutgoService {

    @Autowired
    private OutgoMapper outgoMapper;

    public PageQueryDto queryByPage(PageQuery pageQuery) {
        PageQueryDto pageQueryDto = new PageQueryDto();
        outgoMapper.queryByPage(pageQuery);
        pageQueryDto.setTotalRow(pageQuery.getTotalRow());
        pageQueryDto.setData(pageQuery.getList());
        return pageQueryDto;
    }

    public void save(Outgo outgo) {
        outgoMapper.insert(outgo);
    }

    public int modify(Outgo outgo) {
        return outgoMapper.updateTemplateById(outgo);
    }

    public int remove(String id) {
        return outgoMapper.deleteById(id);
    }

    public Outgo detail(String id) {
        return outgoMapper.single(id);
    }
}
