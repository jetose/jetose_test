package com.jetose.service.finms;

import com.jetose.dto.PageQueryDto;
import com.jetose.entity.finms.Income;
import org.beetl.sql.core.engine.PageQuery;

/**
 * Created by Administrator on 2017/9/5.
 */
public interface IncomeService {

    PageQueryDto queryByPage(PageQuery pageQuery);

    void save(Income income);

    int modify(Income income);

    int remove(String id);

    Income detail(String id);

}
