sample
===
* 注释

	select #use("cols")# from FIN_INCOME where #use("condition")#

cols
===

	income_id,income_date,amount,remark,create_id,create_time

updateSample
===

	`income_id`=#incomeId#,`income_date`=#incomeDate#,`amount`=#amount#,`remark`=#remark#,`create_id`=#createId#,`create_time`=#createTime#

condition
===

	1 = 1  
	@if(!isEmpty(incomeDate)){
	 and `income_date`=#incomeDate#
	@}
	@if(!isEmpty(amount)){
	 and `amount`=#amount#
	@}
	@if(!isEmpty(remark)){
	 and `remark`=#remark#
	@}
	@if(!isEmpty(createId)){
	 and `create_id`=#createId#
	@}
	@if(!isEmpty(createTime)){
	 and `create_time`=#createTime#
	@}

queryByPage
===

    select 
    @pageTag() {
        #use("cols")#
    @}
    from FIN_INCOME 
    where #use("condition")# 
    

	
