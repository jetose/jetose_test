sample
===
* 注释

	select #use("cols")# from FIN_OUTGO where #use("condition")#

cols
===

	outgo_id,outgo_date,amount,remark,create_id,create_time

updateSample
===

	`outgo_id`=#outgoId#,`outgo_date`=#outgoDate#,`amount`=#amount#,`remark`=#remark#,`create_id`=#createId#,`create_time`=#createTime#

condition
===

	1 = 1  
	@if(!isEmpty(outgoDate)){
	 and `outgo_date`=#outgoDate#
	@}
	@if(!isEmpty(amount)){
	 and `amount`=#amount#
	@}
	@if(!isEmpty(remark)){
	 and `remark`=#remark#
	@}
	@if(!isEmpty(createId)){
	 and `create_id`=#createId#
	@}
	@if(!isEmpty(createTime)){
	 and `create_time`=#createTime#
	@}
	
queryByPage
===

    select 
    @pageTag() {
        #use("cols")#
    @}
    from FIN_OUTGO 
    where #use("condition")# 