drop table if exists SYS_USER;

/*==============================================================*/
/* Table: SYS_USER                                              */
/*==============================================================*/
create table SYS_USER
(
   user_id              varchar(36) not null comment '主键ID',
   account               varchar(36) comment '登录账号',
   password             varchar(20) comment '登录密码',
   name                 varchar(50) not null comment '用户名称',
   sex                  varchar(2) not null comment '用户性别',
   remark               text comment '用户备注',
   create_id            varchar(36) not null comment '操作人',
   create_time          timestamp not null default CURRENT_TIMESTAMP comment '操作时间',
   primary key (user_id)
);

alter table SYS_USER comment '系统用户信息表';
