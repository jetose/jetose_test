/*==============================================================*/
/* Table: FIN_INCOME                                            */
/*==============================================================*/
create table FIN_INCOME
(
   income_id            varchar(36) not null comment '主键ID',
   income_date          timestamp not null comment '收入日期',
   amount               numeric(10,2) not null comment '收入金额',
   remark               text comment '收入备注',
   create_id            varchar(36) not null comment '操作人',
   create_time          timestamp not null default CURRENT_TIMESTAMP comment '操作时间',
   primary key (income_id)
);

alter table FIN_INCOME comment '财务收入明细表';

/*==============================================================*/
/* Table: FIN_OUTGO                                             */
/*==============================================================*/
create table FIN_OUTGO
(
   outgo_id             varchar(36) not null comment '主键ID',
   outgo_date           timestamp not null comment '支出日期',
   amount               numeric(10,2) not null comment '支出金额',
   remark               text comment '支出备注',
   create_id            varchar(36) not null comment '操作人',
   create_time          timestamp not null default CURRENT_TIMESTAMP comment '操作时间',
   primary key (outgo_id)
);

alter table FIN_OUTGO comment '财务支出明细表';
