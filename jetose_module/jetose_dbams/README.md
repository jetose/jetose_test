# jetose_dbams
jetose_功能模块_数据管理

## 使用指南
- 拷贝所需模块的sql文件到resources/db/migration
- 调整文件版本号

## 数据结构
```
db.migration
├── sysms -- 系统管理
├── finms -- 财务管理
```

## 数据模型
### sysms -- 系统管理
![系统管理模型](db/migration/sysms/jetose_sysms.png)
### finms -- 财务管理
![财务管理模型](db/migration/finms/jetose_finms.png)