# jetose_springboot
jetose_核心模块_以spring-boot为核心搭建

## 使用指南
- 生成jar包
```
mvn clean package
```
- 上传到本地maven仓库
```
mvn install:install-file -Dfile=jetose_springboot-2017.0.0.1.jar -DgroupId=com.jetose -DartifactId=jetose_springboot -Dversion=2017.0.0.1 -Dpackaging=jar
```

## 技术选型
### 后端技术
- spring-boot -- 微型架构，[http://projects.spring.io/spring-boot](http://projects.spring.io/spring-boot)
- beetl -- 模板引擎，[http://ibeetl.com](http://ibeetl.com)
- beetlsql -- 数据访问，[http://ibeetl.com](http://ibeetl.com)
- druid -- 连接监控，[https://github.com/alibaba/druid](https://github.com/alibaba/druid)
- fastjson -- 解析生成，[https://github.com/alibaba/fastjson](https://github.com/alibaba/fastjson)
- flyway -- 数据移植，[https://flywaydb.org](https://flywaydb.org)
- lombok -- 消除冗长，[https://projectlombok.org](https://projectlombok.org)
- swagger2 -- 文档服务，[http://springfox.github.io/springfox](http://springfox.github.io/springfox)
### 前端技术
- layui -- 前端框架，[http://www.layui.com](http://www.layui.com)

## 规范约定
- annotation类 -- 注解类，需要在名为annotation的包下，并以Annotation结尾，如PageQueryAnnotation
- aop类 -- 切面类，需要在名为aop的包下，并以Aop结尾，如PageQueryAop
- configuration类 -- 配置类，需要在名为configuration的包下，并以Configuration结尾，如BeetlConfiguration
- generate类 -- 生成类，需要在名为generate的包下，并以Generate结尾，如BeetlSQLGenerate
- constant类 -- 常量类，需要在名为constant的包下，并以Constant结尾，如FinmsConstant
- entity类 -- 实体类，需要在名为entity的包下，并以表名去掉前缀命名，如User
- dto类 -- 显示类，需要在名为dto的包下，并以Dto结尾，如PageQueryDto
- mapper类 -- 数据类，需要在名为mapper的包下，并以Mapper结尾，如UserMapper
- service类 -- 业务类，需要在名为service的包下，并以Service结尾，如UserService
- resource类 -- 控制类，需要在名为resource的包下，并以Resource结尾，如UserResource

## 附件
### 在线文档
- [spring-boot](https://docs.spring.io/spring-boot/docs/1.5.7.RELEASE/reference/htmlsingle)
- [beetl](http://ibeetl.com/guide/#beetl)
- [beetlsql](http://ibeetl.com/guide/#beetlsql)
- [druid](https://github.com/alibaba/druid/wiki)
- [fastjson](https://github.com/alibaba/fastjson/wiki)
- [flyway](https://flywaydb.org/documentation/)
- [lombok](http://jnb.ociweb.com/jnb/jnbJan2010.html)
- [swagger2](http://springfox.github.io/springfox/docs/current)