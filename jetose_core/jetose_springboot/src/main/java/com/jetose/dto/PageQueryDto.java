package com.jetose.dto;

import lombok.Data;

import java.util.List;

/**
 * 分页查询显示类
 * @author Jef_Wang
 */
@Data
public class PageQueryDto<T> {

    private int status;

    private String message;

    private long totalRow;

    private List<T> data;

}
