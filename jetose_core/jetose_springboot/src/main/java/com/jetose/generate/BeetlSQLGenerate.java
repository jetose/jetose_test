package com.jetose.generate;

import org.beetl.sql.core.*;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.gen.GenConfig;
import org.beetl.sql.ext.gen.GenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * BeetlSQL代码生成类
 * @author Jef_Wang
 */
@Service
public class BeetlSQLGenerate {

    @Value("${beetlsql.table: jetose_}")
    private String tables;

    @Value("${beetlsql.package: com.jetose}")
    private String packages;

    @Value("${beetlsql.sql: /sql}")
    private String sqls;

    @Autowired
    private DataSource dataSource;

    public void getBeetlSQLGenerate() throws Exception {
        MySqlStyle mySqlStyle = new MySqlStyle();
        SQLLoader sqlLoader = new ClasspathLoader(sqls.substring(1));
        MySqlConnectionSource mySqlContectionSource = new MySqlConnectionSource();
        NameConversion nameConversion = new UnderlinedNameConversion();
        Interceptor[] interceptors = new Interceptor[]{new DebugInterceptor()};
        SQLManager sqlManager = new SQLManager(mySqlStyle, sqlLoader, mySqlContectionSource, nameConversion, interceptors);
        GenConfig genConfig = new GenConfig();
        genConfig.setPreferDate(true);
        sqlManager.genALL(packages + ".entity", genConfig, new GenFilter(){
            @Override
            public boolean accept(String tableName) {
                if (tableName.startsWith(tables)) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    class MySqlConnectionSource implements ConnectionSource {

        public Connection getMaster() {
            return this.getConnection();
        }

        public Connection getSlave() {
            return this.getMaster();
        }

        public Connection getConn(String s, boolean b, String s1, List<?> list) {
            return this.getConnection();
        }

        public void forceBegin(boolean b) {

        }

        public void forceEnd() {

        }

        public boolean isTransaction() {
            return false;
        }

        private Connection getConnection() {
            Connection connection = null;
            try {
                connection = dataSource.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return connection;
        }
    }

}
