package com.jetose.configuration;

import org.beetl.sql.core.IDAutoGen;
import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * BeetlSQL生成ID规则配置类
 * @author Jef_Wang
 */
@Configuration
public class BeetlSQLIdConfiguration {

    @Autowired
    private SQLManager sqlManager;

    @PostConstruct
    public void addUuidIdAutoGen() {
        sqlManager.addIdAutonGen("uuid", new IDAutoGen() {
            public Object nextID(String s) {
                return UUID.randomUUID().toString();
            }
        });
    }

}
