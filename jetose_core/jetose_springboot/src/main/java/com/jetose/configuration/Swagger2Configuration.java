package com.jetose.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置类
 * @author Jef_Wang
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    @Bean
    public Docket createRestApi(Environment environment) {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo(environment))
                .select()
                .apis(RequestHandlerSelectors.basePackage(environment.getProperty("swagger2.package")))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo getApiInfo(Environment environment) {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title(environment.getProperty("project.title"))
                .description(environment.getProperty("project.description"))
                .contact(new Contact(environment.getProperty("project.author"), environment.getProperty("project.website"), environment.getProperty("project.email")))
                .version(environment.getProperty("project.version"))
                .build();
        return apiInfo;
    }

}
