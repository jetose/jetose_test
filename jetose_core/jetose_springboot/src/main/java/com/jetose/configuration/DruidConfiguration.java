package com.jetose.configuration;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Druid配置类
 * @author Jef_Wang
 */
@Configuration
public class DruidConfiguration {

    @Bean
    public ServletRegistrationBean getDruidStatViewServlet(Environment environment) {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        servletRegistrationBean.addInitParameter("allow", environment.getProperty("druid.allow"));
        servletRegistrationBean.addInitParameter("deny", environment.getProperty("druid.deny"));
        servletRegistrationBean.addInitParameter("loginUsername", environment.getProperty("druid.login-username"));
        servletRegistrationBean.addInitParameter("loginPassword", environment.getProperty("druid.login-password"));
        servletRegistrationBean.addInitParameter("resetEnable", environment.getProperty("druid.reset-enable"));
        return servletRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean getDruidStatFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
        return filterRegistrationBean;
    }

}
