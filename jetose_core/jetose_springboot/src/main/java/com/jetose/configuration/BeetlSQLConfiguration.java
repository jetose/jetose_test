package com.jetose.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.spring4.BeetlSqlDataSource;
import org.beetl.sql.ext.spring4.BeetlSqlScannerConfigurer;
import org.beetl.sql.ext.spring4.SqlManagerFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

/**
 * BeetlSQL配置类
 * @author Jef_Wang
 */
@Configuration
public class BeetlSQLConfiguration {

    @Bean(name = "dataSource")
    public DataSource getDruidDataSource(Environment environment) {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(environment.getProperty("spring.datasource.driver-class-name"));
        druidDataSource.setUrl(environment.getProperty("spring.datasource.url"));
        druidDataSource.setUsername(environment.getProperty("spring.datasource.username"));
        druidDataSource.setPassword(environment.getProperty("spring.datasource.password"));
        druidDataSource.setValidationQuery(environment.getProperty("spring.datasource.validation-query"));
        druidDataSource.setInitialSize(Integer.parseInt(environment.getProperty("spring.datasource.initial-size")));
        druidDataSource.setMaxActive(Integer.parseInt(environment.getProperty("spring.datasource.max-active")));
        return druidDataSource;
    }

    @Bean(name = "sqlManagerFactoryBean")
    @Primary
    public SqlManagerFactoryBean getSqlManagerFactoryBean(@Qualifier("dataSource") DataSource dataSource, Environment environment) {
        SqlManagerFactoryBean sqlManagerFactoryBean = new SqlManagerFactoryBean();
        BeetlSqlDataSource beetlSqlDataSource = new BeetlSqlDataSource();
        beetlSqlDataSource.setMasterSource(dataSource);
        sqlManagerFactoryBean.setCs(beetlSqlDataSource);
        sqlManagerFactoryBean.setDbStyle(new MySqlStyle());
        sqlManagerFactoryBean.setInterceptors(new Interceptor[]{new DebugInterceptor()});
        sqlManagerFactoryBean.setNc(new UnderlinedNameConversion());
        sqlManagerFactoryBean.setSqlLoader(new ClasspathLoader(environment.getProperty("beetlsql.sql")));
        return sqlManagerFactoryBean;
    }

    @Bean(name = "beetlSqlConfig")
    public BeetlSqlScannerConfigurer getBeetlSqlScannerConfigurer(Environment environment) {
        BeetlSqlScannerConfigurer beetlSqlScannerConfigurer = new BeetlSqlScannerConfigurer();
        beetlSqlScannerConfigurer.setBasePackage(environment.getProperty("beetlsql.package"));
        beetlSqlScannerConfigurer.setDaoSuffix("Mapper");
        beetlSqlScannerConfigurer.setSqlManagerFactoryBeanName("sqlManagerFactoryBean");
        return beetlSqlScannerConfigurer;
    }

}
