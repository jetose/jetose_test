package com.jetose.aop;

import com.alibaba.fastjson.JSON;
import com.jetose.annotation.PageQueryAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 分页查询切面类
 * @author Jef_Wang
 */
@Aspect
@Component
@Slf4j
public class PageQueryAop {

    @Pointcut("execution(* com.jetose.resources..*(..))")
    public void pointCut() {
    }

    @Before(value = "pointCut()")
    public void before(JoinPoint pjp) throws Exception {
        Object[] args = pjp.getArgs();
        for (Object arg : args) {
            if (arg instanceof PageQuery) {
                Object target = pjp.getTarget();
                String className = target.getClass().getName();
                String methodName = pjp.getSignature().getName();
                Method method = target.getClass().getMethod(methodName, PageQuery.class);
                Annotation[][] annotations = method.getParameterAnnotations();
                for (Annotation[] anns : annotations) {
                    for (Annotation ann: anns) {
                        if (ann instanceof PageQueryAnnotation) {
                            PageQuery pageQuery = (PageQuery) arg;
                            Object paras = pageQuery.getParas();
                            if (paras != null) {
                                pageQuery.setParas(JSON.parseObject(paras.toString(), Map.class));
                            }
                            log.debug(className + " " + methodName + " - " + JSON.toJSONString(pageQuery));
                        }
                    }
                }
            }
        }
    }

}
