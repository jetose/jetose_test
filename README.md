# Nacos基础教程(一)：使用Nacos实现服务注册与发现

## 什么是Nacos

Nacos致力于帮助您发现、配置和管理微服务。Nacos提供了一组简单易用的特性集，帮助您快速实现动态服务发现、服务配置、服务元数据及流量管理。Nacos帮助您更敏捷和容易地构建、交付和管理微服务平台。Nacos是构建以“服务”为中心的现代应用架构 (例如微服务范式、云原生范式) 的服务基础设施。

## 安装Nacos

### 下载地址

https://github.com/alibaba/nacos/releases

### 当前版本

2019.04.03 - 1.0.0-RC3

### 安装步骤

* 下载nacos-server-1.0.0-RC3.zip或nacos-server-1.0.0-RC3.tar.gz