layui.define(['layer', 'element', 'table', 'form', 'userApi'], function(exports){

    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element;
    var table = layui.table;
    var form = layui.form;
    var userApi = layui.userApi;

    var funcs = {
        init: function () {
            initTable(userApi);
            listenTable(userApi);
            listenForm(userApi);
        }
    };

    var initTable = function(apis) {
        table.render({
            id: 'userTable',
            elem: '#user', //绑定元素
            url: apis.query.url, //接口地址
            cols: [[
                {field: 'name', title: '用户名称', width: 120},
                {field: 'sex', title: '用户性别', width: 100},
                {field: 'account', title: '登录账号', width: 120},
                {field: 'password', title: '登录密码', width: 120},
                {field: 'remark', title: '用户备注', width: 400},
                {title: '操作相关', fixed: 'right', width: 180, toolbar: '#btns'} //这里的toolbar值是模板元素的选择器
            ]], //设置表头
            initSort: { //初始排序
                field: 'name', //排序字段，对应 cols 设定的各字段名
                type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            request: {
                pageName: 'pageNumber', //页码的参数名称，默认：page
                limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }, //请求参数page、limit重新设定
            response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName:"totalRow" //数据总数的字段名称，默认：count
            },
            page: true, //是否开启分页
            limits: [20, 50, 100], //每页数据量可选项
            limit: 20, //默认每页数据量
            loading: true //是否显示加载条
        });
    };

    var listenTable = function(apis) {
        table.on('tool(user)', function(obj) {
            var data = obj.data; //获得当前行数据
            var event = obj.event; //获得 lay-event 对应的值
            if (event == 'modify') { //编辑
                enable();
                $("#userId").val(data.userId);
                $("#name").val(data.name);
                $("#sex").val(data.sex);
                $("#account").val(data.account);
                $("#password").val(data.password);
                $("#remark").val(data.remark);
            } else if (event == 'remove') { //删除
                layer.confirm('确认要删除此收入明细记录吗？', function(index){
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url: apis.remove.url.replace('{id}', data.userId),
                        contentType: 'application/json; charset=UTF-8',
                        type: apis.remove.type,
                        dataType: 'json',
                        success: function(resp){
                            reload(apis);
                        },
                        error: function(e) {

                        }
                    });
                });
            } else if (event == 'detail') { //详情
                layer.msg(event);
                disable();
                $("#userId").val(data.userId);
                $("#name").val(data.name);
                $("#sex").val(data.sex);
                $("#account").val(data.account);
                $("#password").val(data.password);
                $("#remark").val(data.remark);
            }
        });
    };

    var listenForm = function(apis) {
        form.on('submit(submitBtn)', function(data) {
            var url, type;
            // 暂时替换create_id，等做了认证之后修改这里
            data.field.create_id = 'Jef_Wang';
            if (data.field.userId == null || data.field.userId == '') {
                url = apis.save.url;
                type = apis.save.type;
            } else {
                url = apis.modify.url.replace('{id}', data.field.userId);
                type = apis.modify.type;
            }
            $.ajax({
                url: url,
                contentType: 'application/json; charset=UTF-8',
                type: type,
                dataType: 'json',
                data: JSON.stringify(data.field),
                success: function(resp){
                    reset();
                    reload(apis);
                },
                error: function(e) {

                }
            });
            return false;
        });
    };

    // 重载表格
    var reload = function(apis){
        table.reload('userTable', {
            url: apis.query.url
        })
    };

    // 启用
    var enable = function() {
        $("#userId").attr('disabled', false);
        $("#name").attr('disabled', false);
        $("#sex").attr('disabled', false);
        $("#account").attr('disabled', false);
        $("#password").attr('disabled', false);
        $("#remark").attr('disabled', false);
    };

    // 禁用
    var disable = function() {
        $("#userId").attr('disabled', true);
        $("#name").attr('disabled', true);
        $("#sex").attr('disabled', true);
        $("#account").attr('disabled', true);
        $("#password").attr('disabled', true);
        $("#remark").attr('disabled', true);
    };

    // 重置
    var reset = function() {
        $("#userId").val('').attr('disabled', false);
        $("#name").val('').attr('disabled', false);
        $("#sex").val('').attr('disabled', false);
        $("#account").val('').attr('disabled', false);
        $("#password").val('').attr('disabled', false);
        $("#remark").val('').attr('disabled', false);
    };

    exports('userCtrl', funcs); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});