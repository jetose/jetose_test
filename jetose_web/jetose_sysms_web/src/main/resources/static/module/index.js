//一般直接写在一个js文件中
layui.define(['layer', 'element'], function(exports){
    var layer = layui.layer;
    var element = layui.element;

    layer.msg('Hello Index');

    exports('index', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});