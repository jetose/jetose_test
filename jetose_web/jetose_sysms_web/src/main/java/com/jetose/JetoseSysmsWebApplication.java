package com.jetose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JetoseSysmsWebApplication {

    public static void main(String[] args){
        SpringApplication.run(JetoseSysmsWebApplication.class, args);
    }

}
