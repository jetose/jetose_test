# jetose_finms_web
jetose_页面模块_财务管理

## 使用指南
- 完成所需模块的README.md使用指南
- 拷贝所需模块的sql文件到resources/db/migration
- 调整文件版本号
- 生成jar包
```
mvn clean package
```
- 运行jar包
```
java -jar jetose_finms_web.jar
```

## 模块依赖
- jetose_springboot -- jetose_核心模块_以spring-boot为核心搭建
- jetose_sysms -- jetose_功能模块_系统管理

## 开发进度
- v2017.0.0.1 -- 完成收入管理、支出管理基础功能开发