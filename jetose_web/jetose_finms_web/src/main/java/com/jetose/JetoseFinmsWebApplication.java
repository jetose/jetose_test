package com.jetose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017/10/10.
 */
@SpringBootApplication
public class JetoseFinmsWebApplication {

    public static void main(String[] args){
        SpringApplication.run(JetoseFinmsWebApplication.class, args);
    }

}
