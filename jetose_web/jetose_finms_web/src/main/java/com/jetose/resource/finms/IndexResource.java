package com.jetose.resource.finms;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/views/v1")
public class IndexResource {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView indexView = new ModelAndView("/index.html");
        return indexView;
    }

    @RequestMapping(value = "/income", method = RequestMethod.GET)
    public ModelAndView income() {
        ModelAndView indexView = new ModelAndView("/finms/income.html");
        return indexView;
    }

    @RequestMapping(value = "/outgo", method = RequestMethod.GET)
    public ModelAndView outgo() {
        ModelAndView indexView = new ModelAndView("/finms/outgo.html");
        return indexView;
    }

}
