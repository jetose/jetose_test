layui.define(function(exports) {

    var $ = layui.jquery;
    var ctxPath = $("#ctxPath").val();

    var apis = {
        // 查询数据api
        'query': {
            'url': ctxPath + '/apis/v1/outgos',
            'type': 'GET'
        },
        // 新增数据api
        'save': {
            'url': ctxPath + '/apis/v1/outgos',
            'type': 'POST'
        },
        // 编辑数据api
        'modify': {
            'url': ctxPath + '/apis/v1/outgos/{id}',
            'type': 'PUT'
        },
        // 删除数据api
        'remove': {
            'url': ctxPath + '/apis/v1/outgos/{id}',
            'type': 'DELETE'
        },
        // 详情数据api
        'detail': {
            'url': ctxPath + '/apis/v1/outgos/{id}',
            'type': 'GET'
        }
    };

    exports('outgoApi', apis); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致

});