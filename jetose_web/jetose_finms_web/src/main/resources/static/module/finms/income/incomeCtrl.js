layui.define(['layer', 'element', 'table', 'form', 'incomeApi'], function(exports){

    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element;
    var table = layui.table;
    var form = layui.form;
    var incomeApi = layui.incomeApi;

    var funcs = {
        init: function () {
            initTable(incomeApi);
            listenTable(incomeApi);
            listenForm(incomeApi);
        }
    };

    var initTable = function(apis) {
        table.render({
            id: 'incomeTable',
            elem: '#income', //绑定元素
            url: apis.query.url, //接口地址
            cols: [[
                {field: 'incomeDate', title: '收入日期', width: 150},
                {field: 'amount', title: '收入金额', width: 120},
                {field: 'remark', title: '收入备注', width: 800},
                {title: '操作相关', fixed: 'right', width: 180, toolbar: '#btns'} //这里的toolbar值是模板元素的选择器
            ]], //设置表头
            initSort: { //初始排序
                field: 'incomeDate', //排序字段，对应 cols 设定的各字段名
                type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            request: {
                pageName: 'pageNumber', //页码的参数名称，默认：page
                limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }, //请求参数page、limit重新设定
            response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName:"totalRow" //数据总数的字段名称，默认：count
            },
            page: true, //是否开启分页
            limits: [20, 50, 100], //每页数据量可选项
            limit: 20, //默认每页数据量
            loading: true //是否显示加载条
        });
    };

    var listenTable = function(apis) {
        table.on('tool(income)', function(obj) {
            var data = obj.data; //获得当前行数据
            var event = obj.event; //获得 lay-event 对应的值
            if (event == 'modify') { //编辑
                enable();
                $("#incomeId").val(data.incomeId);
                $("#incomeDate").val(data.incomeDate);
                $("#amount").val(data.amount);
                $("#remark").val(data.remark);
            } else if (event == 'remove') { //删除
                layer.confirm('确认要删除此收入明细记录吗？', function(index){
                    layer.close(index);
                    //向服务端发送删除指令
                    $.ajax({
                        url: apis.remove.url.replace('{id}', data.incomeId),
                        contentType: 'application/json; charset=UTF-8',
                        type: apis.remove.type,
                        dataType: 'json',
                        success: function(resp){
                            reload(apis);
                        },
                        error: function(e) {

                        }
                    });
                });
            } else if (event == 'detail') { //详情
                layer.msg(event);
                disable();
                $("#incomeId").val(data.incomeId);
                $("#incomeDate").val(data.incomeDate);
                $("#amount").val(data.amount);
                $("#remark").val(data.remark);
            }
        });
    };

    var listenForm = function(apis) {
        form.on('submit(submitBtn)', function(data) {
            var url, type;
            // 暂时替换create_id，等做了认证之后修改这里
            data.field.create_id = 'Jef_Wang';
            if (data.field.incomeId == null || data.field.incomeId == '') {
                url = apis.save.url;
                type = apis.save.type;
            } else {
                url = apis.modify.url.replace('{id}', data.field.incomeId);
                type = apis.modify.type;
            }
            $.ajax({
                url: url,
                contentType: 'application/json; charset=UTF-8',
                type: type,
                dataType: 'json',
                data: JSON.stringify(data.field),
                success: function(resp){
                    reset();
                    reload(apis);
                },
                error: function(e) {

                }
            });
            return false;
        });
    };

    // 重载表格
    var reload = function(apis){
        table.reload('incomeTable', {
            url: apis.query.url
        })
    };

    // 启用
    var enable = function() {
        $("#incomeId").attr('disabled', false);
        $("#incomeDate").attr('disabled', false);
        $("#amount").attr('disabled', false);
        $("#remark").attr('disabled', false);
    };

    // 禁用
    var disable = function() {
        $("#incomeId").attr('disabled', true);
        $("#incomeDate").attr('disabled', true);
        $("#amount").attr('disabled', true);
        $("#remark").attr('disabled', true);
    };

    // 重置
    var reset = function() {
        $("#incomeId").val('').attr('disabled', false);
        $("#incomeDate").val('').attr('disabled', false);
        $("#amount").val('').attr('disabled', false);
        $("#remark").val('').attr('disabled', false);
    };

    exports('incomeCtrl', funcs); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});